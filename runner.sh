#!/bin/bash

inputTimes=$1

inputFiles="out.txt"

var=1


for file in $inputFiles; do
    echo -n "running in $file"
    for ((i = 0; i < ${inputTimes}; i++)); do
        cat "$file" | ./a.out 1 >>tmp.txt
        tail -8 tmp.txt >>result$var.txt
        echo "" >>result$var.txt
        rm tmp.txt
        echo -n "."
    done
    var=$(($var + 1))
    echo ""
done

echo "end"
