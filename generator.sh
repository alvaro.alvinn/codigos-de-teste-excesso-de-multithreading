#!/bin/bash

baseCount=$1 # quantidade de diretórios no diretório raiz
deep=$2   # profundidade
files=$3  # qunatidade de arquivos por diretório
dirs=$4   # qunaitdade de diretórios por diretório(principal)

#detmRandom1=(47 12 68 34 92 56 23 78 5 42 88 17 71 63 9 30 84 52 2 76 41 95 20 64 11 87 50 37 72 26 98 70 16 54 32 79 8 65 29 93 44 75 19 61 14 83 38 81 7 48 27 69 10 59 24 99 4 66 45 89 21 57 31 85 15 73 6 94 40 91 35 67 22 80 18 53 74 3 58 28 96 13 49 36 82 25 60 46 1 86 55 33 97 62 39 90 51 77 43)
#detmRandom2=(76 31 52 9 64 20 85 41 56 13 68 97 24 49 77 36 60 16 45 84 1 29 72 89 7 40 61 17 83 8 55 32 93 74 5 22 69 12 78 3 65 46 88 28 80 57 11 38 81 25 96 59 4 23 50 87 35 70 2 19 66 44 92 27 54 14 98 75 48 10 33 79 6 21 95 18 53 71 42 15 34 62 90 58 91 26 67 39 73 99 37 94 63 30 47 58)

diretorios=0
arquivos=0

#para a quantidade de diretórios iniciais
for ((i = 0; i < ${baseCount}; i++)); do
    #vai crianndo diretórios até chegar na ultima camada
    for ((j = 0; j < ${deep}; j++)); do
        echo "mkdir dir${i}_${j}" >>out.txt
        diretorios=$(($diretorios + 1))
        #echo "" >>out.txt
        echo "cd dir${i}_${j}" >>out.txt
        #echo "" >>out.txt
    done
    #vai votlando diretrório por diretório até o inicio
    for ((j = 0; j < ${deep}; j++)); do
        #em cada camada cria mais diretórios
        for ((k = 0; k < ${dirs}; k++)); do
            echo "mkdir dir${j}_${k}" >>out.txt
            diretorios=$(($diretorios + 1))
            #echo "" >>out.txt
            echo "cd dir${j}_${k}" >>out.txt
            #echo "" >>out.txt
            #em cada diretórios adiciona arquivos
            for ((l = 0; l < ${dirs}; l++)); do
                echo "touch file${j}_${k}_${l}" >>out.txt
                arquivos=$(($arquivos + 1))
                #echo "" >>out.txt
            done
            echo "cd .." >>out.txt
            #echo "" >>out.txt
        done
        #ou arquivos
        for ((k = 0; k < ${files}; k++)); do
            echo "touch file${j}_${k}" >>out.txt
            arquivos=$(($arquivos + 1))
            #echo "" >>out.txt
        done
        echo "cd .." >>out.txt
        #echo "" >>out.txt
    done
done

echo "run" >>out.txt

echo "q" >>out.txt
echo "" >>out.txt

echo "diretórios: $diretorios"
echo "arquivos: $arquivos"
