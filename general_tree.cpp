// CPP program to do level order traversal
// of a generic tree
#include <bits/stdc++.h>
#include <boost/algorithm/string.hpp>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>

#define THREADSTACK 65536

using namespace std;

sem_t semaforo;

sem_t semaforo_tempo;

pthread_attr_t attr;


int threads_count = 8;
int threads_being_used = 1;
int total_threads_created = 1;
int max_threads_in_use = 1;

int u_threads_count = 8;
int u_threads_being_used = 1;
int u_total_threads_created = 1;
int u_max_threads_in_use = 1;


static double tempo_total_all = 0.0;

static double u_tempo_total_all = 0.0;

static double s_tempo_total_all = 0.0;

bool hasAvaliableThread(){
    //printf("número de threads em uso: %d\n", threads_being_used);
    return threads_being_used < threads_count;
}

void releseThread(){
    sem_wait(&semaforo); // Aguarda disponibilidade do semáforo
    //printf(" - liberando thread! %d -> %d\n", threads_being_used, threads_being_used - 1);
    threads_being_used--;
    sem_post(&semaforo); // Libera o semáforo
    pthread_exit(NULL);
    return;
}

void aquireThread(){
    //printf(" + Adquirindo thread! %d -> %d\n", threads_being_used, threads_being_used + 1);
    threads_being_used++;
    max_threads_in_use = max(threads_being_used, max_threads_in_use);
    total_threads_created++;
    return;
}

bool u_hasAvaliableThread(){
    //("número de threads em uso: %d\n", u_threads_being_used);
    return u_threads_being_used < u_threads_count;
}

void u_releseThread(){
    sem_wait(&semaforo); // Aguarda disponibilidade do semáforo
    //printf(" - liberando thread! %d -> %d\n", u_threads_being_used, u_threads_being_used - 1);
    u_threads_being_used--;
    sem_post(&semaforo); // Libera o semáforo
    pthread_exit(NULL);
    return;
}

void u_aquireThread(){
    //printf(" + Adquirindo thread! %d -> %d\n", u_threads_being_used, u_threads_being_used + 1);
    u_threads_being_used++;
    u_max_threads_in_use = max(u_threads_being_used, u_max_threads_in_use);
    u_total_threads_created++;
    return;
}

static struct timespec global_begin, global_end;

static struct timespec u_global_begin, u_global_end;

static struct timespec s_global_begin, s_global_end;
  
// Represents a node of an n-ary tree
struct Node
{
    string key;
    vector<Node *>child;
    Node *parent;
};
struct ThreadFindInDirInput{
    Node* dir;
    char* name;
    bool isNewThread;
};

static Node* root;
static Node* current_node;
  
 // Utility function to create a new tree node
Node *newNode(string key)
{
    Node *temp = new Node;
    temp->key = key;
    //strcpy(temp->key,key);
    return temp;
}

void addTo(Node * parent, string newNodeKey){
    (parent->child).push_back(newNode(newNodeKey));
    (parent->child).back()->parent = parent;
}
 
// Prints the n-ary tree level wise
void LevelOrderTraversal(Node * root)
{
    if (root==NULL)
        return;
  
    // Standard level order traversal code
    // using queue
    queue<Node *> q;  // Create a queue
    q.push(root); // Enqueue root 
    while (!q.empty())
    {
        int n = q.size();
 
        // If this node has children
        while (n > 0)
        {
            // Dequeue an item from queue and print it
            Node * p = q.front();
            q.pop();
            cout << p->key << " ";
  
            // Enqueue all children of the dequeued item
            for (int i=0; i<p->child.size(); i++)
                q.push(p->child[i]);
            n--;
        }
  
        cout << endl; // Print new line between two levels
    }
}

int cd(string in2){
    if(in2.compare("..") == 0){
        if(current_node->parent != NULL){
            current_node = current_node->parent;
            std::cout << "voltou" << std::endl;
            return 1;
        }
        else{
            std::cout << "ja no raiz" << std::endl;
            return 1; 
        }
        
    }
    else{
        for(int i = 0; i< current_node->child.size(); i++){
            if(current_node->child[i]->key.compare(in2) == 0){
                current_node = current_node->child[i];
                std::cout << "foi" << std::endl;
                return 1;
            }
        }
    }
    return 0;
}

int mkdir(string in2){
    addTo(current_node, in2);
    return 1;
}

int touch(string in2){
    addTo(current_node, in2);
    return 1;
}

int findInDirSimple(Node* dir, string name){
    // se é um diretórios
    if(dir->child.size() > 0){
        // para cada entrada
        for(int i = 0; i< dir->child.size(); i++){
            // verrifica se achou
            if(dir->child[i]->key.compare(name) == 0){
                std::cout << "achou!" << std::endl;
            }
            // se é um diretório procura de novo
            if(dir->child[i]->child.size() > 0){
                findInDirSimple(dir->child[i],name);
            }
        }
    }
    return 0;
}

int find(string in2){

    clock_gettime(CLOCK_REALTIME, &s_global_begin);

    findInDirSimple(root, in2);

    clock_gettime(CLOCK_REALTIME, &s_global_end);
    long seconds = s_global_end.tv_sec - s_global_begin.tv_sec;
    long nanoseconds = s_global_end.tv_nsec - s_global_begin.tv_nsec;
    double elapsed = seconds + nanoseconds*1e-9;

    s_tempo_total_all = elapsed;

    printf("!Total_time_single: %.10f\n", s_tempo_total_all);



    return 1;
}

void * findInDirLimited(void* input){
    struct ThreadFindInDirInput* threadArg = (struct ThreadFindInDirInput*)input;
    Node* dir = threadArg->dir;
    string name(threadArg->name);
    bool isNewThread = threadArg->isNewThread;
    pthread_t thread_id;
    // se é um diretórios
    if(dir->child.size() > 0){
        // para cada entrada
        for(int i = 0; i < dir->child.size(); i++){
            // verrifica se achou
            if(dir->child[i]->key.compare(name) == 0){
                std::cout << "achou!" << std::endl;
            }
            // se é um diretório procura de novo
            if(dir->child[i]->child.size() > 0){
                sem_wait(&semaforo); // Aguarda disponibilidade do semáforo
                if(hasAvaliableThread()){
                    aquireThread();
                    struct ThreadFindInDirInput* threadArg = (struct ThreadFindInDirInput*)malloc(sizeof(struct ThreadFindInDirInput));
                    threadArg->dir = dir->child[i];
                    threadArg->name = (char*)malloc(name.length() * sizeof(char) + 1);
                    strcpy(threadArg->name, name.c_str());
                    threadArg->isNewThread = true;
                    pthread_create(&thread_id, &attr, findInDirLimited, threadArg);                   
                    sem_post(&semaforo); // Libera o semáforo
                }
                else{
                    sem_post(&semaforo); // Libera o semáforo
                    struct ThreadFindInDirInput* threadArg = (struct ThreadFindInDirInput*)malloc(sizeof(struct ThreadFindInDirInput)) ;
                    threadArg->dir = dir->child[i];
                    threadArg->name = (char*)malloc(name.length() * sizeof(char) + 1);
                    strcpy(threadArg->name, name.c_str());
                    threadArg->isNewThread = false;
                    findInDirLimited(threadArg);
                }
                //printf("Thread ID: %ld\n",thread_id);   
            }
        }
    }
    free(((ThreadFindInDirInput*)input)->name);
    free(input);

    clock_gettime(CLOCK_REALTIME, &global_end);
    long seconds = global_end.tv_sec - global_begin.tv_sec;
    long nanoseconds = global_end.tv_nsec - global_begin.tv_nsec;
    double elapsed = seconds + nanoseconds*1e-9;

    sem_wait(&semaforo_tempo); // Aguarda disponibilidade do semáforo
    tempo_total_all = elapsed;
    sem_post(&semaforo_tempo); // Libera o semáforo

    if(isNewThread) releseThread();
    return NULL;
}

int lfind(string in2){

    clock_gettime(CLOCK_REALTIME, &global_begin);

    struct ThreadFindInDirInput* threadArg = (struct ThreadFindInDirInput*)malloc(sizeof(struct ThreadFindInDirInput)) ;
    threadArg->dir = root;
    threadArg->name = (char*)malloc(in2.length() * sizeof(char) + 1);
    threadArg->isNewThread = false;
    strcpy(threadArg->name, in2.c_str());
    findInDirLimited(threadArg);

    while(threads_being_used > 1){
    }
    return 1;
}

void * findInDirUnlimited(void* input){
    struct ThreadFindInDirInput* threadArg = (struct ThreadFindInDirInput*)input;
    Node* dir = threadArg->dir;
    string name(threadArg->name);
    bool isNewThread = threadArg->isNewThread;
    pthread_t thread_id = 0;;
    // se é um diretórios
    if(dir->child.size() > 0){
        // para cada entrada
        for(int i = 0; i< dir->child.size(); i++){
            // verrifica se achou
            if(dir->child[i]->key.compare(name) == 0){
                std::cout << "achou!" << std::endl;
            }
            // se é um diretório procura de novo
            if(dir->child[i]->child.size() > 0){
                sem_wait(&semaforo); // Aguarda disponibilidade do semáforo
                u_aquireThread();
                struct ThreadFindInDirInput* threadArg = (struct ThreadFindInDirInput*)malloc(sizeof(struct ThreadFindInDirInput));
                threadArg->dir = dir->child[i];
                threadArg->name = (char*)malloc(name.length() * sizeof(char) + 1);
                strcpy(threadArg->name, name.c_str());
                threadArg->isNewThread = true;
                pthread_create(&thread_id, &attr, findInDirUnlimited, threadArg);                      
                sem_post(&semaforo); // Libera o semáforo
                //printf("Thread ID: %ld\n",thread_id);   
            }
        }
    }
    free(((ThreadFindInDirInput*)input)->name);
    free(input);

    clock_gettime(CLOCK_REALTIME, &u_global_end);
    long seconds = u_global_end.tv_sec - u_global_begin.tv_sec;
    long nanoseconds = u_global_end.tv_nsec - u_global_begin.tv_nsec;
    double elapsed = seconds + nanoseconds*1e-9;

    sem_wait(&semaforo_tempo); // Aguarda disponibilidade do semáforo
    u_tempo_total_all = elapsed;
    sem_post(&semaforo_tempo); // Libera o semáforo

    if(isNewThread) u_releseThread();
    return NULL;
}

int ufind(string in2){

    clock_gettime(CLOCK_REALTIME, &u_global_begin);

    struct ThreadFindInDirInput* threadArg = (struct ThreadFindInDirInput*)malloc(sizeof(struct ThreadFindInDirInput)) ;
    threadArg->dir = root;
    threadArg->name = (char*)malloc(in2.length() * sizeof(char) + 1);
    threadArg->isNewThread = false;
    strcpy(threadArg->name, in2.c_str());
    findInDirUnlimited(threadArg);

    while(u_threads_being_used > 1){
        //printf("%d ", u_threads_being_used);
    }
    return 1;
}



int exit(){

    sem_destroy(&semaforo);
    sem_destroy(&semaforo);
    
    //printf("\nLIMITED:\n");
    //printf("!threads_count: %d\n", threads_count);
    ////printf("threads_being_used: %d\n", threads_being_used);
    //printf("!total_threads_created: %d\n", total_threads_created);
    //printf("!max_threads_in_use: %d\n", max_threads_in_use);
    //printf("!Total_time: %.10f\n", tempo_total_all);
    //printf("UNLIMITED:\n");
    //printf("!threads_count: %d\n", u_threads_count);
    ////printf("threads_being_used: %d\n", u_threads_being_used);
    //printf("!total_threads_created: %d\n", u_total_threads_created);
    //printf("!max_threads_in_use: %d\n", u_max_threads_in_use);
    //printf("!Total_time: %.10f\n", u_tempo_total_all);
    return 1;
}

void run(int times){
    for(int i = 0; i<times; i++){
        threads_being_used = 1;
        total_threads_created = 1;
        max_threads_in_use = 1;


        lfind("bla");
        //printf("\nLIMITED:\n");
        printf("!threads_count: %d\n", threads_count);
        //printf("threads_being_used: %d\n", threads_being_used);
        printf("!total_threads_created: %d\n", total_threads_created);
        printf("!max_threads_in_use: %d\n", max_threads_in_use);
        printf("!Total_time: %.10f\n", tempo_total_all);


        u_threads_being_used = 1;
        u_total_threads_created = 1;
        u_max_threads_in_use = 1;

        ufind("bla");
        //printf("UNLIMITED:\n");
        printf("!threads_count: %d\n", u_threads_count);
        //printf("threads_being_used: %d\n", u_threads_being_used);
        printf("!total_threads_created: %d\n", u_total_threads_created);
        printf("!max_threads_in_use: %d\n", u_max_threads_in_use);
        printf("!Total_time: %.10f\n", u_tempo_total_all);
    }
    return;
}

// Driver program
int main(int argc, char *argv[])
{
    int times = atoi(argv[1]);

    pthread_attr_init(&attr);
    pthread_attr_setstacksize(&attr, THREADSTACK);    

    root = newNode("raiz");
  
    cout << "Level order traversal Before Mirroring\n";
    LevelOrderTraversal(root);
    
    char inputBuffer[__INT16_MAX__];
    sem_init(&semaforo, 0, 1); // Inicializa o semáforo com valor 1 (binário)
    sem_init(&semaforo_tempo, 0, 1); // Inicializa o semáforo com valor 1 (binário)    

    current_node = root;

    std::vector<string> in;

    bool quit = false;
    while(!quit){
        //printf("%s/$ ", current_node->key.c_str());

        std::cin.clear();
        string input;
        std::getline (std::cin,input);     
        boost::split(in, input, boost::is_any_of(" "));
        if(in.size() != 0){

            if (in[0].compare("q") == 0) 
            {
                quit = true;
                exit();
            } 
            else if (in[0].compare("help") == 0)
            {   
                printf("Comandos disponíveis:\n\n");
                printf("q        - Sair\n");
                printf("cd       - Navegar até diretório selecionado\n");
                printf("mkdir    - Cria novo diretório\n");
                printf("touch    - Cria um arquivo\n");
                printf("find     - Encontrar arquivos/diretórios com base em tags e nomes\n");
                printf("lfind    - Encontrar arquivos/diretórios com base no nome (limita threads à maquina)\n");
                printf("ufind    - Encontrar arquivos/diretórios com base no nome (não limita threads à maquina)\n");
                printf("\n");
            }
            else if (in[0].compare("cd") == 0)
            {
                cd(in[1]);
            }
            else if (in[0].compare("mkdir") == 0)
            {
                mkdir(in[1]);
            }
            else if (in[0].compare("touch") == 0)
            {
                touch(in[1]);
            }
            else if (in[0].compare("lfind") == 0)
            {
                lfind(in[1]);
            }
            else if (in[0].compare("ufind") == 0)
            {
                ufind(in[1]);
            }
            else if (in[0].compare("find") == 0)
            {
                find(in[1]);
            }
            else if (in[0].compare("run") == 0)
            {
                run(times);
            }
            else
            {
                printf("comando não reconhecido! digite help para obter os comandos disponíveis!\n");
            }
        }

    }
    return 0;
} 


